// 1. Create a class function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console 
//   `${name} moving to ${topSpeed}`

class SpaceShip {
    constructor(name, topSpeed) {
        this.name = name;
        this.topSpeed = topSpeed;
        // this.accelerate = console.log(`${name} moving to ${topSpeed}`);
    }
    accelerate() {
        const{name, topSpeed} = this;
        console.log(`${name} moving to ${topSpeed}`);
    }
}

// 2. Call the constructor with a couple ships, 
// and call accelerate on both of them.

const enterprise = new SpaceShip("Enterprise-D", "warp 9.6");
const voyager = new SpaceShip("Voyager", "warp 9.975");
const defiant = new SpaceShip("Defiant", "warp 9.5");
enterprise.accelerate();
voyager.accelerate();
defiant.accelerate();