// 1. Create attack function below.  This will take the following parameters:
// attackingPlayer, defendingPlayer, baseDamage, variableDamage

// let round = 1;

// let attack = function(attackingPlayer, defendingPlayer, baseDamage, variableDamage) {

//   //generate random number
//   let rendomNum = function (min, max) {
//     min = Math.ceil(min);
//     max = Math.floor(max);
//     return Math.floor(Math.random() * (max - min + 1)) + min;
//   }
//   //Calculate total damage
//   totalDamage = baseDamage + rendomNum(0, variableDamage);

//   //Reduce the health property of the defendingPlayer  
//   defendingPlayer.health -= totalDamage;

//   //Meta info
//   console.log(`======================`);
//   console.log("ROUND:", round);
//   round++
//   console.log(`======================`);
//   console.log(`Total damage to ${defendingPlayer.name}:`, totalDamage);
//   console.log(`Current score:`);
//   console.log(attackingPlayer.name, ", health:", attackingPlayer.health);
//   console.log(defendingPlayer.name, ", health:", defendingPlayer.health);

// }


// 2. Create player1 and player2 objects below
// Each should have a name property of your choosing, and health property equal to 10
let player1 = new Object();
player1.name = "Eddie";
player1.health = 10; 

let player2 = new Object();
player2.name = "Tim";
player2.health = 10; 

// 3. Refactor attack function to an arrow function.  Comment out function above.

let roundCount = 1;

let attack = (attackingPlayer, defendingPlayer, baseDamage, variableDamage) => {

  //Generate random number
  let rendomNum = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  //Calculate total damage
  totalDamage = baseDamage + rendomNum(0, variableDamage);

  //Reduce the health property of the defendingPlayer  
  defendingPlayer.health -= totalDamage;

  //Meta info
  console.log(`======================`);
  console.log("ROUND:", roundCount);
  roundCount++;
  console.log(`======================`);
  console.log(`${attackingPlayer.name} hits ${defendingPlayer.name} for ${totalDamage} damage`);
  console.log(`Current score:`);
  console.log(attackingPlayer.name, ", health:", attackingPlayer.health);
  console.log(defendingPlayer.name, ", health:", defendingPlayer.health);

}

// DO NOT MODIFY THE CODE BELOW THIS LINE
// Set attacker and defender.  Reverse roles each iteration
let attackOrder = [player1, player2];

// Everything related to preventInfiniteLoop would not generally be necessary, just adding to
// safeguard students from accidentally creating an infinite loop & crashing browser
let preventInfiniteLoop = 100;
while (player1.health >= 1 && player2.health >= 1 && preventInfiniteLoop > 0) {
  const [attackingPlayer, defendingPlayer] = attackOrder;
  console.log(attack(attackingPlayer, defendingPlayer, 1, 2));
  attackOrder = attackOrder.reverse();

  preventInfiniteLoop--;
}
const eliminatedPlayer = player1.health <= 0 ? player1 : player2;
console.log(`${eliminatedPlayer.name} has been eliminated!`);